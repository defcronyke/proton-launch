#!/bin/bash
# © Copyright 2020 Jeremy Carter <jeremy@jeremycarter.ca>
#
# LICENSE:
#   Ambiguous Terms. You may use it, but it might not be a
#   good idea, it's up to you.
#
# Launch Proton Steam games from the command line.
# It has only been tested on Debian.
# 
# Requires the jq tool:
#     sudo apt-get install jq
#
# Examples:
# -----
# Retreive this script with wget and run Doom 64 with Proton 5.0:
#     mkdir -p ~/games && cd ~/games && \
#     wget https://gitlab.com/defcronyke/proton-launch/-/raw/master/proton-launch.sh && \
#     chmod 750 proton-launch.sh && \
#     ./proton-launch.sh 'DOOM 64' 'Doom 64' 'DOOM64_x64.exe' '5.0' 'wine'
# -----
# Run Final Fantasy XII The Zodiac Age with Proton 5.0:
#     ./proton-launch.sh 'FINAL FANTASY XII THE ZODIAC AGE' 'FINAL FANTASY XII THE ZODIAC AGE/x64' 'FFXII_TZA.exe' '5.0' 'wine'
# -----
#
# Explanation:
#     The first argument is the game name as published on Steam, for
#     example, for Doom 64, the correct value is uppercase:
#         'DOOM 64'
#
#     The second argument is the folder name where the game binary is,
#     usually somewhere around here, for example:
#         ~/.local/share/Steam/steamapps/common/<Game Name Here>
#     Note that the case is important, and for the example of Doom 64,
#     the correct value is:
#         'Doom 64'
#     And for FFXII, the binary isn't in the game's top-level folder,
#     it's in a subfolder called x64/, so the correct value is:
#         'FINAL FANTASY XII THE ZODIAC AGE/x64'
#
#     The third argument is the filename of the game's main executable 
#     binary file, for example, for Doom 64 the binary is called:
#         'DOOM64_x64.exe'
#     And for Final Fantasy XII The Zodiac Age, the binary is called:
#         'FFXII_TZA.exe'
#     Note that sometimes the game will only start properly if you
#     launch the binary while in the correct directory, in this case
#     you can specify that directory as the third argument, and the 
#     fourth argument can be prefixed with a subfolder name, for
#     example, something like this:
#         ./proton-launch.sh 'FINAL FANTASY IX' 'FINAL FANTASY IX' 'x64/FF9.exe %command%' '4.11' 'proton'
#
#     The fourth argument is the version of Proton you want to use.
#     For example:
#         '5.0'
#     Valid values at time of testing were:
#         '5.0', '4.11', '3.7', or '3.16'
#
#     The fifth argument is the type of launcher to use, currently the valid options are:
#         'proton' or 'wine'
#     If unspecified it defaults to 'wine', since more games seem to work with that one.
#

# Args.
STEAM_GAME="$1"		# 'DOOM 64'
STEAM_GAME_PATH="$2"	# 'Doom 64'
STEAM_GAME_BINARY="$3"	# 'DOOM64_x64.exe'
PROTON_VERSION="$4"	# '5.0', '4.11', '3.7', or '3.16'
LAUNCH_TYPE="$5"	# 'proton' or 'wine'

# Use a discrete PRIME GPU. Uncomment the line 
# below to enable it if you have one of those 
# GPUs, or add that line to your ~/.bashrc file 
# instead if you want everything you launch from 
# a bash terminal to use your discrete GPU.
#export DRI_PRIME=1

# Download and save the game's AppID if we don't have it.
if [ ! -f "$STEAM_GAME.appid" ]; then
	curl -s http://api.steampowered.com/ISteamApps/GetAppList/v0002/ | jq -c ".applist.apps | .[] | select(.name | contains(\"$STEAM_GAME\")) | .appid" | tail -n1 > "$STEAM_GAME.appid"
fi

# The AppID of the game. Proton needs this.
STEAM_GAME_ID=`cat "$STEAM_GAME.appid"`

# Set the Proton version.
export W="$HOME/.steam/steam/steamapps/common/Proton $PROTON_VERSION/dist"

# Set up the Wine environment.
export WINEVERPATH=$W
export PATH=$W/bin:$PATH
export WINESERVER=$W/bin/wineserver
export WINELOADER=$W/bin/wine
export WINEDLLPATH=$W/lib/wine/fakedlls
export LD_LIBRARY_PATH="$W/lib:$LD_LIBRARY_PATH"
export WINEPREFIX=$HOME/.steam/steam/steamapps/compatdata/"$STEAM_GAME_ID"/pfx
export STEAM_COMPAT_DATA_PATH="$HOME/.local/share/Steam/steamapps/compatdata/$STEAM_GAME_ID/pfx"

# Run the game
pwd="$PWD"
cd "$HOME/.local/share/Steam/steamapps/common/$STEAM_GAME_PATH"

if [ "$LAUNCH_TYPE" == 'proton' ]; then
	"$HOME/.steam/steam/steamapps/common/Proton $PROTON_VERSION/proton" run "$HOME/.steam/steam/steamapps/common/$STEAM_GAME_PATH/"$STEAM_GAME_BINARY
else	# If 'wine' or other.
	wine $STEAM_GAME_BINARY
fi

cd "$pwd"

